import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyC7dZaPakNbCm7afNungBrDvnF9DWHaXsU",
    authDomain: "proyectofinal-e7343.firebaseapp.com",
    databaseURL: "https://proyectofinal-e7343-default-rtdb.firebaseio.com",
    projectId: "proyectofinal-e7343",
    storageBucket: "proyectofinal-e7343.appspot.com",
    messagingSenderId: "644113210057",
    appId: "1:644113210057:web:c11595cd7b2086d499cb18"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

// Referencias a los elementos del DOM
const emailInput = document.getElementById("email");
const passwordInput = document.getElementById("contra");
const loginButton = document.getElementById("btnEnviar");
const errorMessage = document.getElementById("error");

// Escuchar el evento de click en el botón de ingreso
loginButton.addEventListener("click", (e) => {
    e.preventDefault(); // Prevenir el comportamiento por defecto de enviar el formulario

    const email = emailInput.value;
    const password = passwordInput.value;

    // Autenticar al usuario
    signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            // Inicio de sesión exitoso
            console.log("¡Inicio de sesión exitoso!");
            // Redireccionar al usuario a la página principal o a donde necesites
            window.location.href = '../html/menu.html';
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(`Error ${errorCode}: ${errorMessage}`);
            // Mostrar mensaje de error al usuario
            const errors = document.querySelector('.error');
            errors.classList.add('errors')
            setTimeout(() => {
                errors.classList.remove('errors');
            }, 2000);
            document.querySelector(".error").textContent = "Intenta de nuevo";
        });
});

// Observar el estado de autenticación
onAuthStateChanged(auth, (user) => {
    if (user) {
        // Usuario está autenticado
        console.log("Usuario autenticado:", user);
        // Puedes redireccionar al usuario a otra página o cargar contenido exclusivo para usuarios autenticados
    } else {
        // Usuario no está autenticado
        console.log("Usuario no autenticado");
    }
});
