import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, ref, set, onChildAdded, onChildRemoved, remove, update, onChildChanged, onValue } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';

// Configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyC7dZaPakNbCm7afNungBrDvnF9DWHaXsU",
    authDomain: "proyectofinal-e7343.firebaseapp.com",
    databaseURL: "https://proyectofinal-e7343-default-rtdb.firebaseio.com",
    projectId: "proyectofinal-e7343",
    storageBucket: "proyectofinal-e7343.appspot.com",
    messagingSenderId: "644113210057",
    appId: "1:644113210057:web:c11595cd7b2086d499cb18"
  };

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);

//Funcion del boton login
btnlogin.addEventListener('click', async function (){
    window.location = "../html/Login.html";
});

// La función deleteProduct ya se proporcionó previamente.
function displayProducts() {
    // Crear contenedor principal y añadirlo al DOM
    const divContainer = document.createElement("div");

    document.getElementById('productList').appendChild(divContainer);

    const productsRef = ref(db, 'perfume');

    // Añadir productos como elementos div individuales dentro del contenedor principal
    onChildAdded(productsRef, snapshot => {
        console.log(`Añadiendo producto con ID ${snapshot.key}`);
        const data = snapshot.val();

        // Crear div para cada propiedad del producto y añadirlos a un div de producto
        const productDiv = document.createElement("div");
        productDiv.classList.add('product');
        productDiv.id = `product-${snapshot.key}`; // ID único para el div del producto

        // Imagen del producto
        const imageDiv = document.createElement("div");
        imageDiv.classList.add('hei');
        const img = document.createElement("img");
        img.classList.add('img');
        imageDiv.classList.add('img_div');
        img.src = data.imageUrl;
        img.alt = data.nombre;
        img.width = 100;
        imageDiv.appendChild(img);
        productDiv.appendChild(imageDiv);

        // Nombre del producto
        const nameDiv = document.createElement("div");
        const h1 = document.createElement('h1');
        h1.classList.add('h1');
        h1.textContent = `${data.nombre}`;
        nameDiv.appendChild(h1);
        productDiv.appendChild(nameDiv);

        // Precio del producto
        const priceDiv = document.createElement("div");
        const p = document.createElement('p');
        p.textContent = `${data.precio}`;
        priceDiv.appendChild(p);
        productDiv.appendChild(priceDiv);

        // Descripción del producto
        const descriptionDiv = document.createElement("div");
        const p2 = document.createElement('p');
        p2.textContent = `${data.descripcion}`;
        descriptionDiv.appendChild(p2);
        productDiv.appendChild(descriptionDiv);

        // Cantidad del producto
        const quantityDiv = document.createElement("div");
        const p3 = document.createElement('p');
        p3.textContent = `${data.cantidad}`;
        quantityDiv.appendChild(p3);
        productDiv.appendChild(quantityDiv);



        // Acciones del producto (botones)
        const actionsDiv = document.createElement("div");


        // Botón Eliminar


        productDiv.appendChild(actionsDiv);
        divContainer.classList.add('add');
        divContainer.classList.add('grid')
        divContainer.classList.add('mar');

        // Añadir el div del producto al contenedor principal
        divContainer.appendChild(productDiv);
    });

    // Deberías manejar onChildChanged y onChildRemoved de manera similar, actualizando o eliminando
    // los divs de productos correspondientes.



    // Actualizar productos
    onChildChanged(productsRef, snapshot => {
        console.log(`Actualizando producto con ID ${snapshot.key}`);
        const data = snapshot.val();
        updateProductInTable(`product-${snapshot.key}`, data);
    });

    // Eliminar productos
    onChildRemoved(productsRef, snapshot => {
        const trToRemove = document.getElementById(`product-${snapshot.key}`);
        if (trToRemove) {
            trToRemove.remove();
        }
    });
}


// Llama a la función para mostrar los productos y habilitar la edición y eliminación en tiempo real
displayProducts();
